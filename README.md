# meli_challenge

Challenge técnico para mercadolibre.

Se utilizo el lenguaje de programacion nodejs para realizar el desafio.

# Pasos de instalacion:

Se debera clonar el repositorio
https://gitlab.com/nicogena90/meli_challenge.git

Es necesario tener instalado npm https://www.npmjs.com/get-npm
Hay que instalar las dependencias, corriendo el comando "npm install" en la raiz del proyecto.

Luego para iniciar la aplicacion se corre el comando npm start

# Ejemplos para probar la app:

Se recomienda utilizar el postman

http://localhost:3000/translate/2morse
con el body:
{
	"text": "HOLA MELI"
}

http://localhost:3000/translate/2text
con el body:
{
	"text": ".... --- .-.. .-  -- . .-.. .."
}

# Se instalaron los siguientes paquetes para acompañar el proyecto:
typescript
express
body-parser
nodemon

# Comentarios del challenge:
No desarrolle la parte de traducir a bits porque no termine de entender el patron de los mensajes,
me base en el ejemplo dado en el texto pero no logre traducirlo a "HOLA MELI" teniendo en cuenta diferentes tamaños de palabra.

