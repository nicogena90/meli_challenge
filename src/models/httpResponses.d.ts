declare namespace ml.httpResponses {

  export const enum code {
    OK = 200,
    BAD_REQUEST = 400,
    FORBIDDEN = 403,
    NOT_FOUND = 404,
    INTERNAL_SERVER_ERROR = 500
  }

  export interface response {
    
    response?: any;
    code: code;
    errorMsg?: string;
  }
  
}