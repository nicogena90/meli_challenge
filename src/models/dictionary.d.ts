declare namespace ml.dictionary {

  export interface morseHuman {
    morseValue: string;
    humanValue: string;
  }
  
}