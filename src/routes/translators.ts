import { Router } from "express";
import * as translatorsCore from "../core/translators";

const api: Router = Router();

// Rutas de acceso al gateway
api.post("/2text/",   translatorsCore.translate2Human);
api.post("/2morse/",  translatorsCore.translate2Morse);

export default api;
