// Componentes
import * as bodyParser from "body-parser";
import * as express from "express";
import { NextFunction, Request, Response } from "express";

// Instanciamos express
const app = express();

// Cargamos la ruta
import translatorsRoutes from "./routes/translators";

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

// Configurar cabeceras
app.use((req: Request, res: Response, next: NextFunction) => {

  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");

  next();
});

// Rutas base
app.use("/translate",    translatorsRoutes);

export default app;
