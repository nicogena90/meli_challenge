import { Response } from "express";

/**
 * Genera y envia una respuesta exitosa
 *
 * @param res           response
 * @param data          datos de retorno
 *
 */
export function success(res: Response, response: any): void {

  const data: ml.httpResponses.response = {
    code: ml.httpResponses.code.OK,
    response
  };

  res.send({ data });
}

/**
 * Genera y envia una respuesta con error
 *
 * @param res           response
 * @param code          codigo del error
 * @param errorMsg      mensaje del error
 * @param errorLogMsg   mensaje del error con stacktrace
 */
export function error(res: Response, code: ml.httpResponses.code, errorMsg: string, errorLogMsg: string): void {

  const response: ml.httpResponses.response = {
    code,
    errorMsg
  };

  console.error(errorMsg, errorLogMsg);
  res.send({ response });
}
