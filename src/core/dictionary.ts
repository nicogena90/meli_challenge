/**
 * Devuelve el diccionario de morse/humano
 *
 */
export function getMorseHumanDictionary(): ml.dictionary.morseHuman[] {

  const dictionary: ml.dictionary.morseHuman[] = 
  [
    {
      morseValue: '.-',
      humanValue: 'A'
    },
    {
      morseValue: '-...',
      humanValue: 'B'
    },
    {
      morseValue: '-.-.',
      humanValue: 'C'
    },
    {
      morseValue: '-..',
      humanValue: 'D'
    },
    {
      morseValue: '.',
      humanValue: 'E'
    },
    {
      morseValue: '..-.',
      humanValue: 'F'
    },
    {
      morseValue: '--.',
      humanValue: 'G'
    },
    {
      morseValue: '....',
      humanValue: 'H'
    },
    {
      morseValue: '..',
      humanValue: 'I'
    },
    {
      morseValue: '.---',
      humanValue: 'J'
    },
    {
      morseValue: '-.-',
      humanValue: 'K'
    },
    {
      morseValue: '.-..',
      humanValue: 'L'
    },
    {
      morseValue: '--',
      humanValue: 'M'
    },
    {
      morseValue: '-.',
      humanValue: 'N'
    },
    {
      morseValue: '---',
      humanValue: 'O'
    },
    {
      morseValue: '.--.',
      humanValue: 'P'
    },
    {
      morseValue: '--.-',
      humanValue: 'Q'
    },
    {
      morseValue: '.-.',
      humanValue: 'R'
    },
    {
      morseValue: '...',
      humanValue: 'S'
    },
    {
      morseValue: '-',
      humanValue: 'T'
    },
    {
      morseValue: '..-',
      humanValue: 'U'
    },
    {
      morseValue: '...-',
      humanValue: 'V'
    },
    {
      morseValue: '.--',
      humanValue: 'W'
    },
    {
      morseValue: '-..-',
      humanValue: 'X'
    },
    {
      morseValue: '-.--',
      humanValue: 'Y'
    },
    {
      morseValue: '--..',
      humanValue: 'Z'
    },
    {
      morseValue: '-----',
      humanValue: '0'
    },
    {
      morseValue: '.----',
      humanValue: '1'
    },
    {
      morseValue: '..---',
      humanValue: '2'
    },
    {
      morseValue: '...--',
      humanValue: '3'
    },
    {
      morseValue: '....-',
      humanValue: '4'
    },
    {
      morseValue: '.....',
      humanValue: '5'
    },
    {
      morseValue: '-....',
      humanValue: '6'
    },
    {
      morseValue: '--...',
      humanValue: '7'
    },
    {
      morseValue: '---..',
      humanValue: '8'
    },
    {
      morseValue: '----.',
      humanValue: '9'
    },
    {
      morseValue: '.-.-.-',
      humanValue: '.'
    }
  ];
  return dictionary;
}
