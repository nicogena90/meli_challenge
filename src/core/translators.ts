import { Request, Response } from "express";
import * as httpResp from "../utils/httpResponses";
import * as hmDictionary from "./dictionary";

/**
 * Recibe un string en MORSE y retorne un string legible por un humano
 *
 * @param req               request
 * @param res               response
 */
export async function translate2Human(req: Request, res: Response): Promise<void> {

  try {

    let translation;
    let sentence = "";

    // Obtenemos los parametros del request
    const { text } = req.body;

    // Se verifica que llegue correctamente el mensaje
    if (text !== undefined) {

      // Sacamos los espacios en blanco
      let words: string[] = text.split('  ');

      // Recorremos las palabras de la oracion a traducir
      for (const word of words) {
        
        // A cada palabra la dividivos para traducir letra por letra
        let letters: string[] = word.split(' ');

        // Recorremos las letras para traducirlas
        for (const letter of letters) {

          // Traducimos una letra
          translation = morseToHuman(letter);

          // Verificamos que se haya traducido correctamente
          if (translation === undefined) {

            // Retornamos el error en la traduccion
            httpResp.error(res, ml.httpResponses.code.INTERNAL_SERVER_ERROR,
              `Palabra '${letter}' no corresponde al diccionario de morse del sistema`,
              `Palabra morse no definida ${letter}`); 
          }

          // Agregamos la letra a la palabra
          sentence += translation;
        }
        sentence += ' ';
      }

      // Sacamos el espacio al final
      sentence = sentence.substring(0, sentence.length - 1);

      // Retornamos la palabra traducida
      httpResp.success(res, sentence);
    } else {

      // Retornamos que el request fue incorrecto
      httpResp.error(res, ml.httpResponses.code.BAD_REQUEST,
        `Mensaje a traducir faltante en los parametros`,
        `Mensaje a traducir undefined`);
    }

  } catch (error) {
    console.log(`${error}`);
  }
}

/**
 * Recibe un string legible por humano y retorne un string en morse
 *
 * @param req               request
 * @param res               response
 */
export async function translate2Morse(req: Request, res: Response): Promise<void> {

  try {

    let translation;
    let sentence = "";

    // Obtenemos los parametros del request
    const { text } = req.body;

    // Se verifica que llegue correctamente el mensaje
    if (text !== undefined) {

      // Sacamos los espacios en blanco
      let words: string[] = text.split(' ');

      // Recorremos las palabras de la oracion a traducir
      for (const word of words) {

        // Recorremos las letras para traducirlas
        for (const letter of word) {

          // Traducimos una letra
          translation = humanToMorse(letter);

          // Verificamos que se haya traducido correctamente
          if (translation === undefined) {

            // Retornamos el error en la traduccion
            httpResp.error(res, ml.httpResponses.code.INTERNAL_SERVER_ERROR,
              `Palabra '${letter}' no corresponde al diccionario de morse del sistema`,
              `Palabra morse no definida ${letter}`); 
          }

          // Agregamos la letra a la palabra
          sentence += `${translation} `;
        }
        sentence += '  ';
      }

      // Sacamos el espacio al final
      sentence = sentence.substring(0, sentence.length - 1);

      // Retornamos la palabra traducida
      httpResp.success(res, sentence);
    } else {

      // Retornamos que el request fue incorrecto
      httpResp.error(res, ml.httpResponses.code.BAD_REQUEST,
        `Mensaje a traducir faltante en los parametros`,
        `Mensaje a traducir undefined`);
    }

  } catch (error) {
    console.log(`${error}`);
  }
}

/**
 * Recibe un string en MORSE y retorne un string legible por un humano
 *
 * @param word               letra a traducir
 */
function morseToHuman(word: string): string | undefined {
  
  // Buscamos la traduccion de la letra en el diccionario
  const result: any = hmDictionary.getMorseHumanDictionary().find(s => s.morseValue === word);

  return (result) ? result.humanValue : result;
}

/**
 * Recibe un string en humano y retorne un string en morse
 *
 * @param word               letra a traducir
 */
function humanToMorse(word: string): string | undefined {
  
  // Buscamos la traduccion de la letra en el diccionario
  const result: any = hmDictionary.getMorseHumanDictionary().find(s => s.humanValue === word);

  return (result) ? result.morseValue : result;
}
